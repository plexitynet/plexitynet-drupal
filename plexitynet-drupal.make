; Drush make file for PlexityNet Drupal 7.x multisites.
api = "2"


; Core
; ----

core = "7.x"
projects[drupal][type] = "core"
projects[drupal][version] = "7.98"
; Patch robots.txt to fix mobile issues reported from Google Search Console.
; http://clients.plexitynet.com/plexitynet/node/2827#comment-11991
projects[drupal][patch][2827] = "http://dev.plexitynet.com/drush-make/pn-d7-robots-txt.patch"
; Patch D7 core to fix PHP 8.2 deprecation issue with panels.
; https://plexitynet.atlassian.net/browse/PN-74
projects[drupal][patch][74] = "http://dev.plexitynet.com/drush-make/pn-d7-fix-php-8.2-deprecation-with-panels.patch"


; Contrib modules
; ---------------

projects[addressfield][type] = "module"
projects[addressfield][subdir] = "contrib"
projects[addressfield][version] = "1.3"

projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc7"

projects[admin_menu_source][type] = "module"
projects[admin_menu_source][subdir] = "contrib"
projects[admin_menu_source][version] = "1.1"

projects[admin_select][type] = "module"
projects[admin_select][subdir] = "contrib"
projects[admin_select][version] = "1.5"
; Fix pass by reference warning to _admin_select_check_access() in PHP 8.0+.
; https://www.drupal.org/project/admin_select/issues/3381114#comment-15191911
projects[admin_select][patch][3381114] = "https://www.drupal.org/files/issues/2023-08-15/3381114-2.patch"

projects[admin_views][type] = "module"
projects[admin_views][subdir] = "contrib"
projects[admin_views][version] = "1.8"

projects[adsense][type] = "module"
projects[adsense][subdir] = "contrib"
projects[adsense][version] = "1.14"

projects[advagg][type] = "module"
projects[advagg][subdir] = "contrib"
projects[advagg][version] = "2.36"

projects[advanced_forum][type] = "module"
projects[advanced_forum][subdir] = "contrib"
projects[advanced_forum][version] = "2.6"

projects[antibot][type] = "module"
projects[antibot][subdir] = "contrib"
projects[antibot][version] = "1.2"

projects[author_pane][type] = "module"
projects[author_pane][subdir] = "contrib"
projects[author_pane][version] = "2.0"

projects[auto_nodetitle][type] = "module"
projects[auto_nodetitle][subdir] = "contrib"
projects[auto_nodetitle][version] = "1.0"

projects[beautytips][type] = "module"
projects[beautytips][subdir] = "contrib"
projects[beautytips][version] = "2.2"

projects[better_exposed_filters][type] = "module"
projects[better_exposed_filters][subdir] = "contrib"
projects[better_exposed_filters][version] = "3.6"

projects[blockreference][type] = "module"
projects[blockreference][subdir] = "contrib"
projects[blockreference][version] = "2.7"

projects[boxes][type] = "module"
projects[boxes][subdir] = "contrib"
projects[boxes][version] = "1.2"

projects[calendar][type] = "module"
projects[calendar][subdir] = "contrib"
projects[calendar][version] = "3.5"

projects[captcha][type] = "module"
projects[captcha][subdir] = "contrib"
projects[captcha][version] = "1.7"

projects[cck][type] = "module"
projects[cck][subdir] = "contrib"
; We're assuming the latest -dev of cck is stable.
projects[cck][version] = "2.x-dev"

projects[ccl][type] = "module"
projects[ccl][subdir] = "contrib"
projects[ccl][version] = "1.7"

projects[charts][type] = "module"
projects[charts][subdir] = "contrib"
projects[charts][version] = "2.1"

projects[ckeditor][type] = "module"
projects[ckeditor][subdir] = "contrib"
projects[ckeditor][version] = "1.23"

projects[ckeditor_link][type] = "module"
projects[ckeditor_link][subdir] = "contrib"
projects[ckeditor_link][version] = "2.4"

projects[clientside_validation][type] = "module"
projects[clientside_validation][subdir] = "contrib"
projects[clientside_validation][version] = "1.47"

projects[coffee][type] = "module"
projects[coffee][subdir] = "contrib"
projects[coffee][version] = "2.3"

projects[colorbox][type] = "module"
projects[colorbox][subdir] = "contrib"
projects[colorbox][version] = "2.17"

projects[comment_goodness][type] = "module"
projects[comment_goodness][subdir] = "contrib"
projects[comment_goodness][version] = "1.4"

projects[comment_notify][type] = "module"
projects[comment_notify][subdir] = "contrib"
projects[comment_notify][version] = "1.3"

projects[commerce][type] = "module"
projects[commerce][subdir] = "contrib"
projects[commerce][version] = "1.17"

projects[commerce_autosku][type] = "module"
projects[commerce_autosku][subdir] = "contrib"
projects[commerce_autosku][version] = "1.2"

projects[commerce_fancy_attributes][type] = "module"
projects[commerce_fancy_attributes][subdir] = "contrib"
projects[commerce_fancy_attributes][version] = "1.0"

projects[commerce_fancy_image_attributes][type] = "module"
projects[commerce_fancy_image_attributes][subdir] = "contrib"
projects[commerce_fancy_image_attributes][version] = "1.3"

projects[commerce_feeds][type] = "module"
projects[commerce_feeds][subdir] = "contrib"
projects[commerce_feeds][version] = "1.4"
; Incompatibility with feeds FeedsProcessor::entityValidate.
; https://www.drupal.org/project/commerce_feeds/issues/3050702#comment-13107262
projects[commerce_feeds][patch][3050702] = "https://www.drupal.org/files/issues/2019-05-15/FeedsProcessor-entityValidate-3050702-1.patch"

projects[commerce_search_api][type] = "module"
projects[commerce_search_api][subdir] = "contrib"
projects[commerce_search_api][version] = "1.6"

projects[conditional_styles][type] = "module"
projects[conditional_styles][subdir] = "contrib"
projects[conditional_styles][version] = "2.2"

projects[contact_google_analytics][type] = "module"
projects[contact_google_analytics][subdir] = "contrib"
projects[contact_google_analytics][version] = "1.4"

projects[content_access][type] = "module"
projects[content_access][subdir] = "contrib"
projects[content_access][version] = "1.2"

projects[context][type] = "module"
projects[context][subdir] = "contrib"
projects[context][version] = "3.12"

projects[context_mobile_detect][type] = "module"
projects[context_mobile_detect][subdir] = "contrib"
projects[context_mobile_detect][version] = "2.0-alpha1"

projects[crumbs][type] = "module"
projects[crumbs][subdir] = "contrib"
projects[crumbs][version] = "2.7"
; PHP 8.1 compatibility for PluginMethodIterator.
; https://www.drupal.org/project/crumbs/issues/3331335#comment-15192022
projects[crumbs][patch][3331335] = "https://www.drupal.org/files/issues/2023-08-15/3331335-3.patch"

projects[css_injector][type] = "module"
projects[css_injector][subdir] = "contrib"
projects[css_injector][version] = "1.10"

projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.21"
; Fix drush rr via aegir causes warning in PHP 8.2.
; https://www.drupal.org/project/ctools/issues/3284043#comment-14546216
projects[ctools][patch][3284043] = "https://www.drupal.org/files/issues/2022-06-03/ctools-3284043-php8-compatibility.patch"

projects[custom_breadcrumbs][type] = "module"
projects[custom_breadcrumbs][subdir] = "contrib"
projects[custom_breadcrumbs][download][type] = "git"
projects[custom_breadcrumbs][download][url] = "https://git.drupalcode.org/project/custom_breadcrumbs.git"
projects[custom_breadcrumbs][download][branch] = "7.x-2.x"
; From a dev version of the 7.x-2.x branch (26/05/2021).
; Latest available -dev version, but most importantly includes fixes for PHP
; 8.2+
projects[custom_breadcrumbs][download][revision] = "c53dc02164320425f928fb3d7187ba2b4b6a0dae"

projects[date][type] = "module"
projects[date][subdir] = "contrib"
projects[date][version] = "2.14"

projects[delta][type] = "module"
projects[delta][subdir] = "contrib"
projects[delta][version] = "3.0-beta9"

projects[devel][type] = "module"
projects[devel][subdir] = "contrib"
projects[devel][version] = "1.7"

projects[diff][type] = "module"
projects[diff][subdir] = "contrib"
projects[diff][version] = "3.4"

projects[ds][type] = "module"
projects[ds][subdir] = "contrib"
projects[ds][version] = "2.16"

projects[ds_bootstrap_layouts][type] = "module"
projects[ds_bootstrap_layouts][subdir] = "contrib"
projects[ds_bootstrap_layouts][version] = "3.1"

projects[email][type] = "module"
projects[email][subdir] = "contrib"
projects[email][version] = "1.3"

projects[email_registration][type] = "module"
projects[email_registration][subdir] = "contrib"
projects[email_registration][version] = "1.5"

projects[entity][type] = "module"
projects[entity][subdir] = "contrib"
projects[entity][version] = "1.10"
; PHP 8.2 - Fix deprecated dynamic properties.
; https://www.drupal.org/project/entity/issues/3328049#comment-15076066
projects[entity][patch][3328049] = "https://git.drupalcode.org/project/entity/-/commit/3fef11d8ec9913f113040f1f72e80f721fdf29d9.patch"

projects[entity2text][type] = "module"
projects[entity2text][subdir] = "contrib"
projects[entity2text][version] = "1.0-alpha3"

projects[entityconnect][type] = "module"
projects[entityconnect][subdir] = "contrib"
projects[entityconnect][version] = "2.0-rc2"

projects[entityform][type] = "module"
projects[entityform][subdir] = "contrib"
projects[entityform][version] = "2.0-rc5"

projects[entityreference][type] = "module"
projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = "1.9"

projects[entity_rules][type] = "module"
projects[entity_rules][subdir] = "contrib"
projects[entity_rules][version] = "1.0-alpha4"

projects[entity_view_mode][type] = "module"
projects[entity_view_mode][subdir] = "contrib"
projects[entity_view_mode][version] = "1.0-rc1"

projects[epsacrop][type] = "module"
projects[epsacrop][subdir] = "contrib"
projects[epsacrop][version] = "2.4"

projects[eva][type] = "module"
projects[eva][subdir] = "contrib"
projects[eva][version] = "1.4"

projects[extlink][type] = "module"
projects[extlink][subdir] = "contrib"
projects[extlink][version] = "1.21"

projects[faq][type] = "module"
projects[faq][subdir] = "contrib"
projects[faq][version] = "1.3"

projects[fb_likebox][type] = "module"
projects[fb_likebox][subdir] = "contrib"
projects[fb_likebox][version] = "2.3"

projects[fboauth][type] = "module"
projects[fboauth][subdir] = "contrib"
projects[fboauth][version] = "2.0-rc3"
; Update available FB API versions (to allow v6.0).
; https://www.drupal.org/project/fboauth/issues/2612868#comment-13547384
projects[fboauth][patch][2612868] = "https://www.drupal.org/files/issues/2020-04-10/2612868-17.patch"
; Support redirect destination via state parameter
; https://www.drupal.org/project/fboauth/issues/2510446#comment-13035898
projects[fboauth][patch][2510446] = "https://www.drupal.org/files/issues/2019-03-23/2510446-27.patch"

projects[features][type] = "module"
projects[features][subdir] = "contrib"
projects[features][version] = "2.15"

projects[features_override][type] = "module"
projects[features_override][subdir] = "contrib"
projects[features_override][version] = "2.0-rc3"

projects[feeds][type] = "module"
projects[feeds][subdir] = "contrib"
projects[feeds][version] = "2.0-beta6"

projects[feeds_tamper][type] = "module"
projects[feeds_tamper][subdir] = "contrib"
projects[feeds_tamper][version] = "1.2"

projects[field_formatter_settings][type] = "module"
projects[field_formatter_settings][subdir] = "contrib"
projects[field_formatter_settings][version] = "1.1"

projects[field_group][type] = "module"
projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.8"

projects[field_permissions][type] = "module"
projects[field_permissions][subdir] = "contrib"
projects[field_permissions][version] = "1.1"

projects[field_slideshow][type] = "module"
projects[field_slideshow][subdir] = "contrib"
projects[field_slideshow][version] = "1.83"

projects[filefield_paths][type] = "module"
projects[filefield_paths][subdir] = "contrib"
projects[filefield_paths][version] = "1.2"

projects[fitvids][type] = "module"
projects[fitvids][subdir] = "contrib"
projects[fitvids][version] = "1.17"

projects[fivestar][type] = "module"
projects[fivestar][subdir] = "contrib"
projects[fivestar][version] = "2.3"

projects[flatcomments][type] = "module"
projects[flatcomments][subdir] = "contrib"
projects[flatcomments][version] = "2.0"

projects[flexslider][type] = "module"
projects[flexslider][subdir] = "contrib"
projects[flexslider][version] = "2.0-rc2"

projects[fontawesome][type] = "module"
projects[fontawesome][subdir] = "contrib"
projects[fontawesome][version] = "2.9"

projects[fontyourface][type] = "module"
projects[fontyourface][subdir] = "contrib"
projects[fontyourface][version] = "2.8"

projects[fpa][type] = "module"
projects[fpa][subdir] = "contrib"
projects[fpa][version] = "2.6"

projects[ga_tokenizer][type] = "module"
projects[ga_tokenizer][subdir] = "contrib"
projects[ga_tokenizer][version] = "1.5"

projects[git_deploy][type] = "module"
projects[git_deploy][subdir] = "contrib"
projects[git_deploy][version] = "2.6"

projects[globalredirect][type] = "module"
projects[globalredirect][subdir] = "contrib"
projects[globalredirect][version] = "1.6"

projects[gmap][type] = "module"
projects[gmap][subdir] = "contrib"
projects[gmap][version] = "2.12"

projects[google_analytics][type] = "module"
projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "2.8"

projects[google_analytics_reports][type] = "module"
projects[google_analytics_reports][subdir] = "contrib"
projects[google_analytics_reports][version] = "3.1"

projects[google_tag][type] = "module"
projects[google_tag][subdir] = "contrib"
projects[google_tag][version] = "1.6"

projects[gravatar][type] = "module"
projects[gravatar][subdir] = "contrib"
projects[gravatar][download][type] = "git"
projects[gravatar][download][url] = "https://git.drupalcode.org/project/gravatar.git"
projects[gravatar][download][branch] = "7.x-1.x"
; From a dev version of the 7.x-1.x branch (01/01/2012).
; Latest available -dev version, but most importantly fixes
; http://drupal.org/node/1387004
projects[gravatar][download][revision] = "bb2f81e6b0e93156ab86e62db70cfe169418cc92"

projects[honeypot][type] = "module"
projects[honeypot][subdir] = "contrib"
projects[honeypot][version] = "1.26"

projects[htmlmail][type] = "module"
projects[htmlmail][subdir] = "contrib"
projects[htmlmail][version] = "2.71"
; Fix PHP 8.2 compatibility: WARNING | Since PHP 7.0, functions inspecting
; arguments, like debug_backtrace().
; Note my patch from https://www.drupal.org/project/htmlmail/issues/3138522#comment-15185162
; is actually only needed for -dev, as we're still using a stable release the
; previous patch is fine.
; https://www.drupal.org/project/htmlmail/issues/3138522#comment-14869370
projects[htmlmail][patch][3138522] = "https://www.drupal.org/files/issues/2023-01-16/htmlmail_3138522.patch"

projects[icon][type] = "module"
projects[icon][subdir] = "contrib"
projects[icon][version] = "1.0"

projects[imagecache_actions][type] = "module"
projects[imagecache_actions][subdir] = "contrib"
projects[imagecache_actions][version] = "1.14"

projects[imagecache_token][type] = "module"
projects[imagecache_token][subdir] = "contrib"
projects[imagecache_token][version] = "1.0-rc2"

projects[imagefield_tokens][type] = "module"
projects[imagefield_tokens][subdir] = "contrib"
projects[imagefield_tokens][version] = "1.12"

projects[image_resize_filter][type] = "module"
projects[image_resize_filter][subdir] = "contrib"
projects[image_resize_filter][version] = "1.16"

projects[imce][type] = "module"
projects[imce][subdir] = "contrib"
projects[imce][version] = "1.11"

projects[imce_filefield][type] = "module"
projects[imce_filefield][subdir] = "contrib"
projects[imce_filefield][version] = "1.1"

projects[imce_mkdir][type] = "module"
projects[imce_mkdir][subdir] = "contrib"
projects[imce_mkdir][version] = "1.0"

projects[imce_tools][type] = "module"
projects[imce_tools][subdir] = "contrib"
projects[imce_tools][version] = "1.2"

projects[imce_wysiwyg][type] = "module"
projects[imce_wysiwyg][subdir] = "contrib"
projects[imce_wysiwyg][version] = "1.0"

projects[include][type] = "module"
projects[include][subdir] = "contrib"
projects[include][version] = "1.8"

projects[inline_conditions][type] = "module"
projects[inline_conditions][subdir] = "contrib"
projects[inline_conditions][version] = "1.0"

projects[inline_entity_form][type] = "module"
projects[inline_entity_form][subdir] = "contrib"
projects[inline_entity_form][version] = "1.9"

projects[insert][type] = "module"
projects[insert][subdir] = "contrib"
projects[insert][version] = "1.4"

projects[invisimail][type] = "module"
projects[invisimail][subdir] = "contrib"
projects[invisimail][version] = "1.2"
; Block & content caching causes incorrect email address substitutions
; (JavaScript replacement method).
; https://www.drupal.org/node/1628032#comment-6106200
projects[invisimail][patch][1628032] = "https://www.drupal.org/files/invisimail-check_markup-caching-id-collisions-1628032-1.patch"

projects[isotope][type] = "module"
projects[isotope][subdir] = "contrib"
projects[isotope][version] = "2.0"
; PHP 8.0+ compatibility: Undefined property $type from isotope.make.
; https://www.drupal.org/project/isotope/issues/3382770#comment-15202628
projects[isotope][patch][3382770] = "https://www.drupal.org/files/issues/2023-08-23/3382770-2.patch"

projects[iss][type] = "module"
projects[iss][subdir] = "contrib"
projects[iss][version] = "1.0-alpha2"

projects[jcarousel][type] = "module"
projects[jcarousel][subdir] = "contrib"
projects[jcarousel][version] = "2.7"

projects[job_scheduler][type] = "module"
projects[job_scheduler][subdir] = "contrib"
projects[job_scheduler][version] = "2.0"

projects[jquery_colorpicker][type] = "module"
projects[jquery_colorpicker][subdir] = "contrib"
projects[jquery_colorpicker][version] = "1.3"

projects[jquery_plugin][type] = "module"
projects[jquery_plugin][subdir] = "contrib"
projects[jquery_plugin][version] = "1.0"

projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "4.1"

projects[less][type] = "module"
projects[less][subdir] = "contrib"
projects[less][version] = "4.0"

projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.5"

projects[link][type] = "module"
projects[link][subdir] = "contrib"
projects[link][version] = "1.11"

projects[linkit][type] = "module"
projects[linkit][subdir] = "contrib"
projects[linkit][version] = "3.6"

projects[location][type] = "module"
projects[location][subdir] = "contrib"
projects[location][version] = "3.7"

projects[login_destination][type] = "module"
projects[login_destination][subdir] = "contrib"
projects[login_destination][version] = "1.4"

projects[logintoboggan][type] = "module"
projects[logintoboggan][subdir] = "contrib"
projects[logintoboggan][version] = "1.5"

projects[mailmime][type] = "module"
projects[mailmime][subdir] = "contrib"
projects[mailmime][download][type] = "git"
projects[mailmime][download][url] = "https://git.drupalcode.org/project/mailmime.git"
projects[mailmime][download][branch] = "7.x-2.x"
; From a dev version of the 7.x-2.x branch (12/09/2021).
; Latest available -dev version, but most importantly includes fixes for PHP
; 8.2+ as per https://www.drupal.org/project/mailmime/issues/2198587
projects[mailmime][download][revision] = "7c933151b465c816716d20ec925d497017182ea7"

projects[mailsystem][type] = "module"
projects[mailsystem][subdir] = "contrib"
projects[mailsystem][version] = "2.35"

projects[masquerade][type] = "module"
projects[masquerade][subdir] = "contrib"
projects[masquerade][version] = "1.0-rc7"

projects[media][type] = "module"
projects[media][subdir] = "contrib"
projects[media][version] = "1.10"

projects[media_gallery][type] = "module"
projects[media_gallery][subdir] = "contrib"
projects[media_gallery][version] = "1.4"

projects[media_youtube][type] = "module"
projects[media_youtube][subdir] = "contrib"
projects[media_youtube][version] = "3.12"

projects[menu_admin_per_menu][type] = "module"
projects[menu_admin_per_menu][subdir] = "contrib"
projects[menu_admin_per_menu][version] = "1.1"

projects[menu_attributes][type] = "module"
projects[menu_attributes][subdir] = "contrib"
projects[menu_attributes][version] = "1.2"

projects[menu_block][type] = "module"
projects[menu_block][subdir] = "contrib"
projects[menu_block][version] = "2.9"

projects[menu_position][type] = "module"
projects[menu_position][subdir] = "contrib"
projects[menu_position][version] = "1.2"

projects[menu_token][type] = "module"
projects[menu_token][subdir] = "contrib"
projects[menu_token][version] = "1.0-beta7"

projects[metatag][type] = "module"
projects[metatag][subdir] = "contrib"
projects[metatag][version] = "1.32"

projects[microdata][type] = "module"
projects[microdata][subdir] = "contrib"
projects[microdata][download][type] = "git"
projects[microdata][download][url] = "https://git.drupalcode.org/project/microdata.git"
projects[microdata][download][branch] = "7.x-1.x"
; From a dev version of the 7.x-1.x branch (10/07/2013).
; Latest available -dev version.
projects[microdata][download][revision] = "6739979bf96bdbe982dc565b1703c3bfebaff1a6"
; Index $titles and $schema_urls by entity_id in microdata_process_page().
; http://drupal.org/node/1875290#comment-6881886
projects[microdata][patch][1875290] = "http://drupal.org/files/1875290-1-entity_id_index.patch"
; Remove theme_microdata_image_formatter() as core theme_image_formatter() now supports attributes.
; http://drupal.org/node/1875346#comment-6882084
projects[microdata][patch][1875346] = "http://drupal.org/files/1875346-1-remove_theme_microdata_image_formatter.patch"
; Fixe errors on features exports.
; https://drupal.org/comment/7875647#comment-7875647
projects[microdata][patch][2092365] = "https://drupal.org/files/microdata-features_export-2092365-1.patch"

projects[mimemail][type] = "module"
projects[mimemail][subdir] = "contrib"
projects[mimemail][version] = "1.2"

projects[module_filter][type] = "module"
projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "2.2"

projects[multiform][type] = "module"
projects[multiform][subdir] = "contrib"
projects[multiform][version] = "1.6"

projects[multiple_node_menu][type] = "module"
projects[multiple_node_menu][subdir] = "contrib"
projects[multiple_node_menu][version] = "1.0-beta2"
; Notice: Undefined index: multiple_node_menu.
; https://www.drupal.org/project/multiple_node_menu/issues/2366795#comment-9313179
projects[multiple_node_menu][patch][2366795] = "https://www.drupal.org/files/issues/2366795.patch"

projects[multiselect][type] = "module"
projects[multiselect][subdir] = "contrib"
projects[multiselect][version] = "1.13"

projects[multiupload_filefield_widget][type] = "module"
projects[multiupload_filefield_widget][subdir] = "contrib"
projects[multiupload_filefield_widget][version] = "1.16"

projects[multiupload_imagefield_widget][type] = "module"
projects[multiupload_imagefield_widget][subdir] = "contrib"
projects[multiupload_imagefield_widget][version] = "1.3"

projects[navbar][type] = "module"
projects[navbar][subdir] = "contrib"
projects[navbar][version] = "1.8"
; Dropdown for navbar horizontal display.
; https://www.drupal.org/project/navbar/issues/2481207#comment-11960235
projects[navbar][patch][2481207] = "https://www.drupal.org/files/issues/dropdown_for_navbar-2481207-22.patch"

projects[navbar_region][type] = "module"
projects[navbar_region][subdir] = "contrib"
projects[navbar_region][version] = "1.0-beta3"

projects[nivo_slider][type] = "module"
projects[nivo_slider][subdir] = "contrib"
projects[nivo_slider][version] = "1.11"

projects[nodeblock][type] = "module"
projects[nodeblock][subdir] = "contrib"
projects[nodeblock][version] = "1.7"

projects[node_embed][type] = "module"
projects[node_embed][subdir] = "contrib"
projects[node_embed][version] = "1.2"

projects[nodequeue][type] = "module"
projects[nodequeue][subdir] = "contrib"
projects[nodequeue][version] = "2.5"

projects[nodereference_url][type] = "module"
projects[nodereference_url][subdir] = "contrib"
projects[nodereference_url][version] = "1.12"

projects[oauth][type] = "module"
projects[oauth][subdir] = "contrib"
projects[oauth][version] = "3.4"
; PHP 7.4 compatibility.
; https://www.drupal.org/project/oauth/issues/3117903
projects[oauth][patch][3117903] = "https://www.drupal.org/files/issues/2020-03-05/oauth-php-7.4.patch"

projects[og][type] = "module"
projects[og][subdir] = "contrib"
projects[og][version] = "2.10"

projects[opengraph_meta][type] = "module"
projects[opengraph_meta][subdir] = "contrib"
projects[opengraph_meta][version] = "2.0-beta3"

projects[options_element][type] = "module"
projects[options_element][subdir] = "contrib"
projects[options_element][version] = "1.12"

projects[override_node_options][type] = "module"
projects[override_node_options][subdir] = "contrib"
projects[override_node_options][version] = "1.15"

projects[page_title][type] = "module"
projects[page_title][subdir] = "contrib"
projects[page_title][version] = "2.7"

projects[panelizer][type] = "module"
projects[panelizer][subdir] = "contrib"
projects[panelizer][version] = "3.4"
; PHP 8.2 Compatibility.
; https://www.drupal.org/project/panelizer/issues/3336759#comment-14889211
projects[panelizer][patch][3336759] = "https://www.drupal.org/files/issues/2023-01-26/panelizer-php81-runtime-3336759-2.patch"

projects[panels][type] = "module"
projects[panels][subdir] = "contrib"
projects[panels][version] = "3.11"
; PHP 8.2 Compatibility.
; https://www.drupal.org/project/panels/issues/3336591#comment-14899480
projects[panels][patch][3336591] = "https://git.drupalcode.org/project/panels/-/commit/0815c56f740455ff124940075277498c4274c9f1.diff"
; Deprecated dynamic properties in panels_renderer_standard in PHP 8.2.
; https://www.drupal.org/project/panels/issues/3382760#comment-15202581
projects[panels][patch][3382760] = "https://www.drupal.org/files/issues/2023-08-23/3382760-2.patch"

projects[paragraphs][type] = "module"
projects[paragraphs][subdir] = "contrib"
projects[paragraphs][version] = "1.0-rc5"

projects[pathauto][type] = "module"
projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.3"

projects[pathologic][type] = "module"
projects[pathologic][subdir] = "contrib"
projects[pathologic][version] = "3.2"

projects[persistent_login][type] = "module"
projects[persistent_login][subdir] = "contrib"
projects[persistent_login][version] = "1.1"

projects[profile2][type] = "module"
projects[profile2][subdir] = "contrib"
projects[profile2][version] = "1.2"

projects[radix_layouts][type] = "module"
projects[radix_layouts][subdir] = "contrib"
projects[radix_layouts][version] = "3.4"

projects[recaptcha][type] = "module"
projects[recaptcha][subdir] = "contrib"
projects[recaptcha][version] = "2.3"

projects[redirect][type] = "module"
projects[redirect][subdir] = "contrib"
projects[redirect][version] = "1.0-rc4"

projects[references][type] = "module"
projects[references][subdir] = "contrib"
projects[references][version] = "2.4"

projects[replicate][type] = "module"
projects[replicate][subdir] = "contrib"
projects[replicate][version] = "1.2"

projects[replicate_paragraphs][type] = "module"
projects[replicate_paragraphs][subdir] = "contrib"
projects[replicate_paragraphs][version] = "1.3"

projects[replicate_ui][type] = "module"
projects[replicate_ui][subdir] = "contrib"
projects[replicate_ui][version] = "1.5"

projects[responsive_menus][type] = "module"
projects[responsive_menus][subdir] = "contrib"
projects[responsive_menus][version] = "1.7"

projects[rules][type] = "module"
projects[rules][subdir] = "contrib"
projects[rules][version] = "2.14"

projects[scheduler][type] = "module"
projects[scheduler][subdir] = "contrib"
projects[scheduler][version] = "1.6"

projects[search404][type] = "module"
projects[search404][subdir] = "contrib"
projects[search404][version] = "1.6"

projects[search_api][type] = "module"
projects[search_api][subdir] = "contrib"
projects[search_api][version] = "1.28"

projects[search_api_db][type] = "module"
projects[search_api_db][subdir] = "contrib"
projects[search_api_db][version] = "1.8"

projects[search_config][type] = "module"
projects[search_config][subdir] = "contrib"
projects[search_config][version] = "1.2"

projects[select_or_other][type] = "module"
projects[select_or_other][subdir] = "contrib"
projects[select_or_other][version] = "2.24"

projects[simplenews][type] = "module"
projects[simplenews][subdir] = "contrib"
projects[simplenews][version] = "1.1"

projects[site_map][type] = "module"
projects[site_map][subdir] = "contrib"
projects[site_map][version] = "1.3"

projects[site_verify][type] = "module"
projects[site_verify][subdir] = "contrib"
projects[site_verify][version] = "1.2"

projects[smtp][type] = "module"
projects[smtp][subdir] = "contrib"
projects[smtp][version] = "1.9"

projects[socialmedia][type] = "module"
projects[socialmedia][subdir] = "contrib"
projects[socialmedia][version] = "1.0-beta16"

projects[spamspan][type] = "module"
projects[spamspan][subdir] = "contrib"
projects[spamspan][version] = "1.3"
; Deprecated passing null to str_replace() in PHP 8.1+.
; https://www.drupal.org/project/spamspan/issues/3382755#comment-15202562
projects[spamspan][patch][3382755] = "https://www.drupal.org/files/issues/2023-08-23/3382755-2.patch"

projects[special_menu_items][type] = "module"
projects[special_menu_items][subdir] = "contrib"
projects[special_menu_items][version] = "2.2"

projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[styleguide][type] = "module"
projects[styleguide][subdir] = "contrib"
projects[styleguide][version] = "1.1"

projects[styles][type] = "module"
projects[styles][subdir] = "contrib"
projects[styles][version] = "2.0-alpha8"

projects[superfish][type] = "module"
projects[superfish][subdir] = "contrib"
projects[superfish][version] = "2.0"
; Don't enable makefile by default.
; Fixes issues with drush make in PHP 8.0+.
; https://www.drupal.org/project/superfish/issues/2196755#comment-8485493
projects[superfish][patch][2196755] = "https://www.drupal.org/files/issues/superfish-rename_makefile-2196755-1.patch"

projects[themekey][type] = "module"
projects[themekey][subdir] = "contrib"
projects[themekey][version] = "3.4"

projects[token][type] = "module"
projects[token][subdir] = "contrib"
projects[token][version] = "1.9"

projects[transliteration][type] = "module"
projects[transliteration][subdir] = "contrib"
projects[transliteration][version] = "3.2"

projects[uuid][type] = "module"
projects[uuid][subdir] = "contrib"
projects[uuid][version] = "1.3"

projects[variable][type] = "module"
projects[variable][subdir] = "contrib"
projects[variable][version] = "2.5"

projects[video_embed_field][type] = "module"
projects[video_embed_field][subdir] = "contrib"
projects[video_embed_field][version] = "2.0-beta11"

projects[viewfield][type] = "module"
projects[viewfield][subdir] = "contrib"
projects[viewfield][version] = "2.2"

projects[views][type] = "module"
projects[views][subdir] = "contrib"
projects[views][version] = "3.29"

projects[views_bootstrap][type] = "module"
projects[views_bootstrap][subdir] = "contrib"
projects[views_bootstrap][version] = "3.5"

projects[views_bulk_operations][type] = "module"
projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.7"

projects[views_data_export][type] = "module"
projects[views_data_export][subdir] = "contrib"
projects[views_data_export][version] = "3.2"

projects[views_php][type] = "module"
projects[views_php][subdir] = "contrib"
projects[views_php][version] = "1.1"

projects[views_responsive_grid][type] = "module"
projects[views_responsive_grid][subdir] = "contrib"
projects[views_responsive_grid][version] = "1.3"

projects[views_slideshow][type] = "module"
projects[views_slideshow][subdir] = "contrib"
projects[views_slideshow][version] = "3.10"

projects[views_tree][type] = "module"
projects[views_tree][subdir] = "contrib"
projects[views_tree][version] = "2.0"

projects[votingapi][type] = "module"
projects[votingapi][subdir] = "contrib"
projects[votingapi][version] = "2.16"

projects[webform][type] = "module"
projects[webform][subdir] = "contrib"
projects[webform][version] = "4.25"

projects[webform_validation][type] = "module"
projects[webform_validation][subdir] = "contrib"
projects[webform_validation][version] = "1.18"

projects[weight][type] = "module"
projects[weight][subdir] = "contrib"
projects[weight][version] = "3.2"

projects[widgets][type] = "module"
projects[widgets][subdir] = "contrib"
projects[widgets][version] = "1.0-rc1"

projects[wysiwyg][type] = "module"
projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.9"

projects[xmlsitemap][type] = "module"
projects[xmlsitemap][subdir] = "contrib"
projects[xmlsitemap][version] = "2.7"


; Custom modules
; --------------

projects[pn_branding][type] = "module"
projects[pn_branding][subdir] = "custom"
projects[pn_branding][download][type] = "git"
projects[pn_branding][download][url] = "git@bitbucket.org:plexitynet/pn_branding.git"
projects[pn_branding][download][branch] = "7.x"

projects[pn_media][type] = "module"
projects[pn_media][subdir] = "custom"
projects[pn_media][download][type] = "git"
projects[pn_media][download][url] = "git@bitbucket.org:plexitynet/pn_media.git"
projects[pn_media][download][branch] = "7.x"

projects[pn_media_responsive][type] = "module"
projects[pn_media_responsive][subdir] = "custom"
projects[pn_media_responsive][download][type] = "git"
projects[pn_media_responsive][download][url] = "git@bitbucket.org:plexitynet/pn_media_responsive.git"
projects[pn_media_responsive][download][branch] = "7.x"

projects[pn_protect_uid1][type] = "module"
projects[pn_protect_uid1][subdir] = "custom"
projects[pn_protect_uid1][download][type] = "git"
projects[pn_protect_uid1][download][url] = "git@bitbucket.org:plexitynet/pn_protect_uid1.git"
projects[pn_protect_uid1][download][branch] = "7.x"

projects[pn_webform][type] = "module"
projects[pn_webform][subdir] = "custom"
projects[pn_webform][download][type] = "git"
projects[pn_webform][download][url] = "git@bitbucket.org:plexitynet/pn_webform.git"
projects[pn_webform][download][branch] = "7.x"

projects[pn_wysiwyg][type] = "module"
projects[pn_wysiwyg][subdir] = "custom"
projects[pn_wysiwyg][download][type] = "git"
projects[pn_wysiwyg][download][url] = "git@bitbucket.org:plexitynet/pn_wysiwyg.git"
projects[pn_wysiwyg][download][branch] = "7.x"

projects[publoc][type] = "module"
projects[publoc][subdir] = "custom"
projects[publoc][download][type] = "git"
projects[publoc][download][url] = "git@bitbucket.org:plexitynet/publoc.git"
projects[publoc][download][branch] = "7.x"

projects[simple_gmap][type] = "module"
projects[simple_gmap][subdir] = "custom"
projects[simple_gmap][download][type] = "git"
projects[simple_gmap][download][url] = "git@bitbucket.org:plexitynet/simple_gmap.git"
projects[simple_gmap][download][branch] = "7.x"


; Custom features
; ---------------

projects[publoc_beer_review_feature][type] = "module"
projects[publoc_beer_review_feature][subdir] = "custom/features"
projects[publoc_beer_review_feature][download][type] = "git"
projects[publoc_beer_review_feature][download][url] = "git@bitbucket.org:plexitynet/publoc_beer_review_feature.git"
projects[publoc_beer_review_feature][download][branch] = "7.x"

projects[publoc_pub_fact_feature][type] = "module"
projects[publoc_pub_fact_feature][subdir] = "custom/features"
projects[publoc_pub_fact_feature][download][type] = "git"
projects[publoc_pub_fact_feature][download][url] = "git@bitbucket.org:plexitynet/publoc_pub_fact_feature.git"
projects[publoc_pub_fact_feature][download][branch] = "7.x"

projects[publoc_pub_feature][type] = "module"
projects[publoc_pub_feature][subdir] = "custom/features"
projects[publoc_pub_feature][download][type] = "git"
projects[publoc_pub_feature][download][url] = "git@bitbucket.org:plexitynet/publoc_pub_feature.git"
projects[publoc_pub_feature][download][branch] = "7.x"


; Contrib themes
; --------------

projects[adaptivetheme][type] = "theme"
projects[adaptivetheme][version] = "3.4"

projects[bootstrap][type] = "theme"
projects[bootstrap][version] = "3.27"

projects[ember][type] = "theme"
projects[ember][version] = "2.0-alpha4"

projects[mayo][type] = "theme"
projects[mayo][version] = "1.4"

projects[omega][type] = "theme"
projects[omega][version] = "3.1"
; Fix PHP Warnings on PHP 7.1.
; https://www.drupal.org/project/omega/issues/2998663#comment-13058396
; Note: disabled as it combined with the patch below in 3381116.
;projects[omega][patch][2998663] = "https://www.drupal.org/files/issues/2019-04-08/2998663-2.patch"
; Deprecated dynamic property $delta in PHP 8.2+.
; https://www.drupal.org/project/omega/issues/3381116#comment-15191922
projects[omega][patch][3381116] = "https://www.drupal.org/files/issues/2023-08-15/3381116-and-2998663-3.patch"
; PHP7.4 notice in views with pager set to display 0 items.
; https://www.drupal.org/project/omega/issues/3166808#comment-15199439
projects[omega][patch][3166808] = "https://www.drupal.org/files/issues/2023-08-21/3166808-4.patch"

projects[porto][type] = "theme"
projects[porto][download][type] = "git"
projects[porto][download][url] = "git@bitbucket.org:plexitynet/porto.git"
projects[porto][download][tag] = "7.x-3.0.1"
; Remove remove non existent prettyPhoto.css and jquery.isotope.css.
; http://clients.plexitynet.com/plexitynet/node/2374#comment-9822
projects[porto][patch][2374] = "http://dev.plexitynet.com/drush-make/2374-porto-remove_nonexistant_prettyphotocss_and_isotopecss.patch"
; ShtterKits porto theme top menu sticky scrolling issue.
; http://clients.plexitynet.com/plexitynet/node/2776#comment-11967
projects[porto][patch][2776] = "http://dev.plexitynet.com/drush-make/2776-porto-fix_sticky_header_issue.patch"

projects[zen][type] = "theme"
projects[zen][version] = "3.3"


; Custom themes
; -------------

projects[publ][type] = "theme"
projects[publ][subdir] = "custom"
projects[publ][download][type] = "git"
projects[publ][download][url] = "git@bitbucket.org:plexitynet/publ.git"
projects[publ][download][branch] = "7.x"


; Libraries
; ---------

libraries[backbone][type] = "library"
libraries[backbone][destination] = "libraries"
libraries[backbone][directory_name] = "backbone"
libraries[backbone][download][type] = "get"
libraries[backbone][download][url] = "https://github.com/jashkenas/backbone/archive/1.0.0.zip"

libraries[colorbox][type] = "library"
libraries[colorbox][destination] = "libraries"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][download][type] = "get"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/1.6.4.zip"

libraries[colorpicker][type] = "library"
libraries[colorpicker][destination] = "libraries"
libraries[colorpicker][directory_name] = "colorpicker"
libraries[colorpicker][download][type] = "get"
libraries[colorpicker][download][url] = "http://www.eyecon.ro/colorpicker/colorpicker.zip"

libraries[fitvids][type] = "library"
libraries[fitvids][destination] = "libraries"
libraries[fitvids][directory_name] = "fitvids"
libraries[fitvids][download][type] = "get"
libraries[fitvids][download][url] = "https://github.com/davatron5000/FitVids.js/archive/v1.2.0.zip"

libraries[flexslider][type] = "library"
libraries[flexslider][destination] = "libraries"
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][download][type] = "get"
libraries[flexslider][download][url] = "https://github.com/woocommerce/FlexSlider/archive/2.6.2.zip"

libraries[fontawesome][type] = "library"
libraries[fontawesome][destination] = "libraries"
libraries[fontawesome][directory_name] = "fontawesome"
libraries[fontawesome][download][type] = "get"
libraries[fontawesome][download][url] = "https://github.com/FortAwesome/Font-Awesome/archive/v4.7.0.zip"

libraries[isotope][type] = "library"
libraries[isotope][destination] = "libraries"
libraries[isotope][directory_name] = "isotope"
libraries[isotope][download][type] = "get"
libraries[isotope][download][url] = "https://unpkg.com/isotope-layout@3.0.6/dist/isotope.pkgd.min.js"

libraries[iframedialog][type] = "library"
libraries[iframedialog][destination] = "libraries"
libraries[iframedialog][directory_name] = "ckeditor/plugins/iframedialog"
libraries[iframedialog][download][type] = "get"
libraries[iframedialog][download][url] = "https://download.ckeditor.com/iframedialog/releases/iframedialog_4.5.11.zip"

libraries[Jcrop][type] = "library"
libraries[Jcrop][destination] = "libraries"
libraries[Jcrop][directory_name] = "Jcrop"
libraries[Jcrop][download][type] = "get"
libraries[Jcrop][download][url] = "https://github.com/tapmodo/Jcrop/archive/v0.9.9.zip"

libraries[jquery_cycle][type] = "library"
libraries[jquery_cycle][destination] = "libraries"
libraries[jquery_cycle][directory_name] = "jquery.cycle"
libraries[jquery_cycle][download][type] = "get"
libraries[jquery_cycle][download][url] = "http://malsup.github.io/jquery.cycle.all.js"
; Aegir 1.10 baulks (who knows why) and complains of jquery.cycle dir isn't
; empty, following avoids the associated drush platform build failure.
libraries[jquery_cycle][overwrite] = 1

libraries[jquery_imagesloaded][type] = "library"
libraries[jquery_imagesloaded][destination] = "libraries"
libraries[jquery_imagesloaded][directory_name] = "jquery.imagesloaded"
libraries[jquery_imagesloaded][download][type] = "get"
; v2.1.2 is required by field_slideshow as per
; https://drupal.org/comment/7757129#comment-7757129
libraries[jquery_imagesloaded][download][url] = "https://github.com/desandro/imagesloaded/archive/v2.1.2.zip"

libraries[jquery_jcarousel][type] = "library"
libraries[jquery_jcarousel][destination] = "libraries"
libraries[jquery_jcarousel][directory_name] = "jquery.jcarousel"
libraries[jquery_jcarousel][download][type] = "get"
;libraries[jquery_jcarousel][download][url] = "http://sorgalla.com/projects/download-zip.php?jcarousel"
; Temp, sorgalla.com is down.
libraries[jquery_jcarousel][download][url] = "http://dev.plexitynet.com/drush-make/jquery.jcarousel-0.2.8.zip"

libraries[json2][type] = "library"
libraries[json2][destination] = "libraries"
libraries[json2][directory_name] = "json2"
libraries[json2][download][type] = "get"
libraries[json2][download][url] = "https://github.com/douglascrockford/JSON-js/archive/master.zip"

libraries[lessphp][type] = "library"
libraries[lessphp][destination] = "libraries"
libraries[lessphp][directory_name] = "lessphp"
libraries[lessphp][download][type] = "get"
libraries[lessphp][download][url] = "https://github.com/wikimedia/less.php/archive/refs/tags/v1.8.2.zip"

libraries[mobile_detect][type] = "library"
libraries[mobile_detect][destination] = "libraries"
libraries[mobile_detect][directory_name] = "Mobile_Detect"
libraries[mobile_detect][download][type] = "get"
libraries[mobile_detect][download][url] = "https://github.com/serbanghita/Mobile-Detect/archive/2.8.41.zip"

libraries[nivo_slider][type] = "library"
libraries[nivo_slider][destination] = "libraries"
libraries[nivo_slider][directory_name] = "nivo-slider"
libraries[nivo_slider][download][type] = "get"
libraries[nivo_slider][download][url] = "https://github.com/gilbitron/Nivo-Slider/archive/3.2.zip"

libraries[modernizr][type] = "library"
libraries[modernizr][destination] = "libraries"
libraries[modernizr][directory_name] = "modernizr"
libraries[modernizr][download][type] = "get"
libraries[modernizr][download][url] = "https://github.com/BrianGilbert/modernizer-navbar/archive/2.7.1.zip"

libraries[superfish][type] = "library"
libraries[superfish][destination] = "libraries"
libraries[superfish][directory_name] = "superfish"
libraries[superfish][download][type] = "get"
libraries[superfish][download][url] = "http://dev.plexitynet.com/drush-make/superfish-library-1.2.zip"

libraries[tinymce][type] = "library"
libraries[tinymce][destination] = "libraries"
libraries[tinymce][directory_name] = "tinymce"
libraries[tinymce][download][type] = "get"
; This library was at http://download.moxiecode.com/tinymce/tinymce_3.5.11.zip
; but that is now a 404, so we host it ourselves.
libraries[tinymce][download][url] = "http://dev.plexitynet.com/drush-make/tinymce_3.5.11.zip"

libraries[underscore][type] = "library"
libraries[underscore][destination] = "libraries"
libraries[underscore][directory_name] = "underscore"
libraries[underscore][download][type] = "get"
libraries[underscore][download][url] = "https://github.com/jashkenas/underscore/archive/1.5.0.zip"


; Custom install profiles
; -----------------------

projects[basic_commerce][type] = "profile"
projects[basic_commerce][download][type] = "git"
projects[basic_commerce][download][url] = "git@bitbucket.org:plexitynet/basic_commerce.git"
projects[basic_commerce][download][branch] = "7.x"

projects[pn_multisite][type] = "profile"
projects[pn_multisite][download][type] = "git"
projects[pn_multisite][download][url] = "git@bitbucket.org:plexitynet/pn_multisite.git"
projects[pn_multisite][download][branch] = "7.x"

